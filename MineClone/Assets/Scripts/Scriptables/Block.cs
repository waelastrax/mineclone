﻿using UnityEngine;

[CreateAssetMenu(menuName = "Blocks/New block")]
public class Block : ScriptableObject
{
    public short type;
    public int maxHealth;

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public short GetType()
    {
        return type;
    }
}

public class BlockReference
{
    Block block;
    int currentHealth;

    public BlockReference(Block bl)
    {
        block = bl;
        currentHealth = bl.GetMaxHealth();
    }
}

/*
public class DummyBlock
{
    private int x, y, z;
    private short blockType;
    private short blockHealth;

    public DummyBlock(int x, int y, int z, short bt)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.blockType = bt;
        SetBlockHealth(blockType);
    }

    private void SetBlockHealth(int bt)
    {
        switch (bt)
        {
            case 1:
                this.blockHealth = 100;
                break;
            case 2:
                this.blockHealth = 1;
                break;
            case 3:
                this.blockHealth = 2;
                break;
            case 4:
                this.blockHealth = 3;
                break;
            default:
                break;
        }
    }

    public bool SameBlock(int newX, int newY, int newZ)
    {
        return newX == x && newY == y && newZ == z ? true : false;
    }

    public void DamageBlock(short damage)
    {
        blockHealth -= damage;
    }

    public bool IsDestroyed()
    {
        return blockHealth == 0 ? true : false;
    }
}
*/

/*
[CreateAssetMenu(menuName = "GlobalVariables/int")]
public class IntVariable : ScriptableObject
{
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif
    public int value;

    public void SetValue(int val)
    {
        value = val;
    }

    public void SetValue(IntVariable val)
    {
        value = val.value;
    }

    public void ApplyChange(int dif)
    {
        value += dif;
    }

    public void ApplyChange(IntVariable dif)
    {
        value += dif.value;
    }
}

[System.Serializable]
public class IntReference
{
    [HideInInspector]
    public bool useConstant = true;
    [HideInInspector]
    public int constantValue;
    [HideInInspector]
    public IntVariable variable;

    public IntReference()
    {

    }

    public int Value
    {
        get { return useConstant ? constantValue : variable.value; }
    }

    public static implicit operator int(IntReference reference)
    {
        return reference.Value;
    }

    public IntReference(int value)
    {
        useConstant = true;
        constantValue = value;
    }
}

[CreateAssetMenu(menuName = "GlobalVariables/float")]
public class FloatVariable : ScriptableObject
{
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif
    public float value;

    public void SetValue(float val)
    {
        value = val;
    }

    public void SetValue(FloatVariable val)
    {
        value = val.value;
    }

    public void ApplyChange(float dif)
    {
        value += dif;
    }

    public void ApplyChange(FloatVariable dif)
    {
        value += dif.value;
    }
}

[System.Serializable]
public class FloatReference
{
    [HideInInspector]
    public bool useConstant = true;
    [HideInInspector]
    public int constantValue;
    [HideInInspector]
    public IntVariable variable;

    public FloatReference()
    {

    }

    public int Value
    {
        get { return useConstant ? constantValue : variable.value; }
    }

    public static implicit operator float(FloatReference reference)
    {
        return reference.Value;
    }

    public FloatReference(int value)
    {
        useConstant = true;
        constantValue = value;
    }
}
     
     
     */





/*
 
     public class DummyBlock
{
    private int x, y, z;
    private short blockType;
    private short blockHealth;

    public DummyBlock(int x, int y, int z, short bt)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.blockType = bt;
        SetBlockHealth(blockType);
    }

    private void SetBlockHealth(int bt)
    {
        switch (bt)
        {
            case 1:
                this.blockHealth = 100;
                break;
            case 2:
                this.blockHealth = 1;
                break;
            case 3:
                this.blockHealth = 2;
                break;
            case 4:
                this.blockHealth = 3;
                break;
            default:
                break;
        }
    }

    public bool SameBlock(int newX, int newY, int newZ)
    {
        return newX == x && newY == y && newZ == z ? true : false;
    }

    public void DamageBlock(short damage)
    {
        blockHealth -= damage;
    }

    public bool IsDestroyed()
    {
        return  blockHealth == 0 ? true : false;
    }
}*/
