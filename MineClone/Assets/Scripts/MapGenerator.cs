﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator
{
    public static float scale = 0.8f;

    public static BlockArray InitBlockArray(int chunkWidth, int chunkHeight, Vector3 chunkPosition)
    {
        int temp;
        BlockArray blocks = new BlockArray(chunkWidth, chunkHeight, chunkWidth);
        ArrayPosition pos = new ArrayPosition(0, 0, 0);

        for (pos.z = 0; pos.z < chunkWidth; pos.z++)
        {
            for (pos.x = 0; pos.x < chunkWidth; pos.x++)
            {
                pos.y = 0;
                blocks.SetBlock(pos, 1); //let's call that bedrock level
                temp = Mathf.FloorToInt(chunkHeight * Mathf.PerlinNoise((chunkPosition.x + pos.x) / chunkWidth, (chunkPosition.z + pos.x) / chunkWidth) * scale);
                //anything up from bedrock should be between values 2 and 4 with those included
                for (pos.y = 1; pos.y < temp; pos.y++)
                {
                    blocks.SetBlock(pos, 1);
                }
            }
        }
        return blocks;
    }

    public static void InitBlocks(short [,,] blocks, int chunkWidth, int chunkHeight, Vector3 chunkPosition)
    {
        int temp;

        for (int z = 0; z < chunkWidth; z++)
        {
            for (int x = 0; x < chunkWidth; x++)
            {
                blocks[x, 0, z] = 1; //let's call that bedrock level
                temp = Mathf.FloorToInt(chunkHeight * Mathf.PerlinNoise((chunkPosition.x + x) / chunkWidth, (chunkPosition.z + z) / chunkWidth) * scale);
                //anything up from bedrock should be between values 2 and 4 with those included
                for (int y = 1; y < temp; y++)
                {
                    blocks[x, y, z] = 4;
                }
            }
        }
    }
}
