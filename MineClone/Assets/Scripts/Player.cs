﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public delegate void CreateBlock(Vector3 cubePos, short blockType);
    CreateBlock createBlock;
    public delegate void DestroyBlock(Vector3 blockPos, short damage);
    DestroyBlock destroyBlock;

    public short blockDamage = 1;
    public short blockType = 1;
    public Transform cubeAdd, cubeDel;
    public Transform world;
    public Text blockSelected;

    private float timer = 0;
    private bool hold = false;
    private bool target = false;
    private bool canPlace = false;


	void Start ()
    {
        Cursor.visible = false;
        createBlock += world.GetComponent<World>().CreateBlock;
        destroyBlock += world.GetComponent<World>().DestroyBlock;
        //cubeAdd.GetComponentInChildren<MeshRenderer>().enabled = false;
        cubeDel.GetComponentInChildren<MeshRenderer>().enabled = false;
        SetText();
    }
	
	void Update ()
    {
        CubeInteraction();

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleDestroyDummy();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            ToggleCreateDummy();
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            blockType -= 1;
            if (blockType < 1) blockType = 4;
            SetText();
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            blockType += 1;
            if (blockType > 4) blockType = 1;
            SetText();
        }

        if (Input.GetMouseButtonDown(0) && target)
        {
            hold = true;
            canPlace = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (timer > 0 && timer < 0.3f && target && canPlace)
            {
                createBlock(cubeAdd.position, blockType);
            }
            timer = 0;
            hold = false;
        }

        if (hold && target)
        {
            timer += Time.deltaTime;
            if (timer > 1)
            {
                canPlace = false;
                destroyBlock(cubeDel.position, blockDamage);
                timer = 0;
            }
        }
        else if (hold && !target)
        {
            timer = 0;
        }
    }

    private void ToggleCreateDummy()
    {
        if (cubeAdd.GetComponentInChildren<MeshRenderer>().enabled)
        {
            cubeAdd.GetComponentInChildren<MeshRenderer>().enabled = false;
        }
        else
        {
            cubeAdd.GetComponentInChildren<MeshRenderer>().enabled = true;
        }
    }

    private void ToggleDestroyDummy()
    {
        if (cubeDel.GetComponentInChildren<MeshRenderer>().enabled)
        {
            cubeDel.GetComponentInChildren<MeshRenderer>().enabled = false;
        }
        else
        {
            cubeDel.GetComponentInChildren<MeshRenderer>().enabled = true;
        }
    }

    private void CubeInteraction()
    {
        Ray ray = new Ray(transform.position + transform.forward / 2, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 6f))
        {
            Vector3 middle = hit.point - hit.normal / 2;
            cubeDel.position = new Vector3(Mathf.Floor(middle.x), Mathf.Floor(middle.y), Mathf.Floor(middle.z));
            middle = hit.point + hit.normal / 2;
            cubeAdd.position = new Vector3(Mathf.Floor(middle.x), Mathf.Floor(middle.y), Mathf.Floor(middle.z));
            target = true;
            //Interact();
        }
        else
        {
            cubeDel.position = new Vector3(0, -100, 0);
            cubeAdd.position = new Vector3(0, -100, 0);
            target = false;
        }
    }

    private void SetText()
    {
        switch (blockType)
        {
            case 1:
                blockSelected.GetComponent<Text>().text = "Stone";
                break;
            case 2:
                blockSelected.GetComponent<Text>().text = "Grass1";
                break;
            case 3:
                blockSelected.GetComponent<Text>().text = "Grass2";
                break;
            case 4:
                blockSelected.GetComponent<Text>().text = "Dirt";
                break;
            default:
                break;
        }
    }
}
