﻿using UnityEngine;

public class DayNightCycle : MonoBehaviour 
{
	#region Declaration
	public bool showGUI = true;
	public Light sun;
	[Range(0,1)]
	public float currentTimeOfDay = 0.2f;
	public float timeMultiplier = 1f; //this thing controls the time
	public float maxMultiplier = 16f;
	public float minMultiplier = 0.25f;

	private float secondsInFullDay = 240f; //day duration if timeMultiplier is set to 1
	private float sunInitialIntensity;
	#endregion

	#region Start & Update
	void Start() 
	{
		sunInitialIntensity = sun.intensity;
	}

	void Update() 
	{
		//update time and sun
		UpdateSun();

		currentTimeOfDay += (Time.deltaTime / secondsInFullDay);

		if (currentTimeOfDay >= 1) 
		{
			currentTimeOfDay = 0;
		}
		//change timeScale - scaling time for everything in the scene
		//if (Time.timeScale != timeMultiplier) Time.timeScale = timeMultiplier;

		//mouse wheel scroll for changing time flow
		/*if (Input.GetAxis("Mouse ScrollWheel") > 0f && timeMultiplier < maxMultiplier)
		{
			timeMultiplier *= 2;
		}
		else if (Input.GetAxis("Mouse ScrollWheel") < 0f && timeMultiplier > minMultiplier)
		{
			timeMultiplier /= 2;
		}
        */
	}
	#endregion

	#region Public
	//returns actual hour
	public int getHour()
	{
		return Mathf.RoundToInt(secondsInFullDay * currentTimeOfDay / 10);
	}
	#endregion

	#region Private
	//updates sun position and light intensity during the day
	void UpdateSun() 
	{
		sun.transform.localRotation = Quaternion.Euler((currentTimeOfDay * 360f) - 90, 170, 0);

		float intensityMultiplier = 1;
		if (currentTimeOfDay <= 0.23f || currentTimeOfDay >= 0.75f) 
		{
			intensityMultiplier = 0;
		}
		else if (currentTimeOfDay <= 0.25f) 
		{
			intensityMultiplier = Mathf.Clamp01((currentTimeOfDay - 0.23f) * (1 / 0.02f));
		}
		else if (currentTimeOfDay >= 0.73f) 
		{
			intensityMultiplier = Mathf.Clamp01(1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));
		}

		sun.intensity = sunInitialIntensity * intensityMultiplier;
	}
		
	//GUI showint time variables
	void OnGUI()
	{
		if (showGUI)
		{
			GUI.Box(new Rect(20,20,170,75), "");
			GUI.Label(new Rect(40,25,150,20), "Day length: " + (secondsInFullDay / timeMultiplier)+ "s");
			GUI.Label(new Rect(40,45,150,20), "Current time: " + Mathf.RoundToInt(secondsInFullDay * currentTimeOfDay / 10).ToString("D2") + "h");
			GUI.Label(new Rect(40,65,150,20), "Time multiplier: " + timeMultiplier);
		}
	}
	#endregion
}