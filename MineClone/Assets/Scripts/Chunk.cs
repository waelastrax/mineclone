﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//make chunk reusable! Instead of instantiating new chunk,
//wipe the data and set up the data again from manager

public class Chunk : MonoBehaviour
{
    private short[,,] blocks;
    public string chunkID; //this should be also used as name of the save
    private int chunkWidth; //we assume, that chunk has same width as length
    private int chunkHeight;

    private Mesh mesh;
    private MeshCollider meshCollider;
    private List<Vector3> newVertices = new List<Vector3>();
    private List<Vector2> newUV = new List<Vector2>();
    private List<int> newTriangles = new List<int>();
    private DummyBlock lastHit;

    #region Init and update
    public void SetChunkProps(string chID, int cWidth, int cHeight, Vector3 position)
    {
        chunkID = chID;
        chunkWidth = cWidth;
        chunkHeight = cHeight;

        transform.position = position;

        blocks = BlockIO.LoadChunkData(chunkID);
        if (blocks == null)
        {
            blocks = new short[chunkWidth, chunkHeight, chunkWidth];
            MapGenerator.InitBlocks(blocks, chunkWidth, chunkHeight, transform.position);
        }

        mesh = new Mesh();
        meshCollider = GetComponent<MeshCollider>();
        GetComponent<MeshFilter>().mesh = mesh;
        GenerateMesh();
    }

    public void UpdateChunk(string chID, Vector3 position)
    {
        chunkID = chID;
        transform.position = position;
        //check for saved data
        GenerateMesh();
    }

    public void CreateBlock(int blockX, int blockY, int blockZ, short blockType)
    {
        Debug.Log("x: " + blockX + " y: " + blockY + " z: " + blockZ);
        if (blocks[blockX, blockY, blockZ] == 0 && blockType != 0)
        {
            blocks[blockX, blockY, blockZ] = blockType;
            BlockIO.SaveChunkData(chunkID, blocks);
            GenerateMesh();
        }
    }

    public void DestroyBlock(int blockX, int blockY, int blockZ, short damage)
    {
        if (blocks[blockX, blockY, blockZ] != 0 && damage > 0)
        {
            if (lastHit == null)
            {
                lastHit = new DummyBlock(blockX, blockY, blockZ, blocks[blockX, blockY, blockZ]);
                lastHit.DamageBlock(damage);
                if (lastHit.IsDestroyed())
                {
                    blocks[blockX, blockY, blockZ] = 0;
                    lastHit = null;
                    BlockIO.SaveChunkData(chunkID, blocks);
                    GenerateMesh();
                }
            }
            else if (lastHit.SameBlock(blockX, blockY, blockZ))
            {
                lastHit.DamageBlock(damage);
                if (lastHit.IsDestroyed())
                {
                    blocks[blockX, blockY, blockZ] = 0;
                    lastHit = null;
                    BlockIO.SaveChunkData(chunkID, blocks);
                    GenerateMesh();
                }
            }
            else
            {
                lastHit = null;
            }
        }
    }

    private int GetChunkX()
    {
        string[] coords = chunkID.Split('#');
        return int.Parse(coords[0]);
    }

    private int GetChunkY()
    {
        string[] coords = chunkID.Split('#');
        return int.Parse(coords[1]);
    }
    #endregion

    #region mesh handling
    private void GenerateMesh()
    {
        mesh.Clear();
        newVertices.Clear();
        newTriangles.Clear();
        newUV.Clear();
        for (int y = 0; y < chunkHeight; y++)
        {
            for (int z = 0; z < chunkWidth; z++)
            {
                for (int x = 0; x < chunkWidth; x++)
                {
                    if (blocks[x, y, z] > 0)
                    {
                        //this does not feel very efficient
                        if (TopSide(x, y, z))
                        {
                            DrawSide(new Vector3(x, y, z), Vector3.forward, Vector3.right, blocks[x, y, z]);
                        }
                        if (BottomSide(x, y, z))
                        {
                            DrawSide(new Vector3(x + 1, y - 1, z), Vector3.forward, Vector3.left, blocks[x, y, z]);
                        }
                        if (BackSide(x, y, z))
                        {
                            DrawSide(new Vector3(x + 1, y, z), Vector3.down, Vector3.left, blocks[x, y, z]);
                        }
                        if (FrontSide(x, y, z))
                        {
                            DrawSide(new Vector3(x, y, z + 1), Vector3.down, Vector3.right, blocks[x, y, z]);
                        }
                        if (LeftSide(x, y, z))
                        {
                            DrawSide(new Vector3(x, y, z), Vector3.down, Vector3.forward, blocks[x, y, z]);
                        }
                        if (RightSide(x, y, z))
                        {
                            DrawSide(new Vector3(x + 1, y, z + 1), Vector3.down, Vector3.back, blocks[x, y, z]);
                        }
                    }
                }
            }
        }
        mesh.vertices = newVertices.ToArray();
        mesh.triangles = newTriangles.ToArray();
        mesh.uv = newUV.ToArray();
        mesh.RecalculateBounds();
        //mesh.RecalculateNormals(); - this screws the mesh totally and royally
        meshCollider.sharedMesh = null;
        meshCollider.sharedMesh = mesh;
    }

    private void DrawSide(Vector3 start, Vector3 sideA, Vector3 sideB, short blockType)
    {
        int index = newVertices.Count;

        newVertices.Add(start);
        newVertices.Add(start + sideA);
        newVertices.Add(start + sideB);
        newVertices.Add(start + sideA + sideB);

        for (int i = 0; i < 3; i++) newTriangles.Add(index + i);
        for (int i = 3; i > 0; i--) newTriangles.Add(index + i);

        switch (blockType)
        {
            case 1: //stone
                newUV.Add(new Vector2(0.5f, 0.5f));
                newUV.Add(new Vector2(0.5f, 1));
                newUV.Add(new Vector2(1, 0.5f));
                newUV.Add(new Vector2(1, 1));
                break;
            case 2: //grass 
                newUV.Add(new Vector2(0, 0));
                newUV.Add(new Vector2(0, 0.5f));
                newUV.Add(new Vector2(0.5f, 0));
                newUV.Add(new Vector2(0.5f, 0.5f));
                break;
            case 3: //grass
                newUV.Add(new Vector2(0, 0.5f));
                newUV.Add(new Vector2(0, 1));
                newUV.Add(new Vector2(0.5f, 0.5f));
                newUV.Add(new Vector2(0.5f, 1));
                break;
            case 4: //dirt
                newUV.Add(new Vector2(0.5f, 0));
                newUV.Add(new Vector2(0.5f, 0.5f));
                newUV.Add(new Vector2(1, 0));
                newUV.Add(new Vector2(1, 0.5f));
                break;
            default:
                break;
        }
    }

    private bool TopSide(int x, int y, int z)
    {
        if (y + 1 < chunkHeight)
        {
            if (blocks[x, y + 1, z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    private bool BottomSide(int x, int y, int z)
    {
        if (y - 1 >= 0)
        {
            if (blocks[x, y - 1, z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    //z + 1
    private bool FrontSide(int x, int y, int z)
    {
        if (z + 1 < chunkWidth)
        {
            if (blocks[x, y, z + 1] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    //z - 1
    private bool BackSide(int x, int y, int z)
    {
        if (z - 1 >= 0)
        {
            if (blocks[x, y, z - 1] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    //x - 1
    private bool LeftSide(int x, int y, int z)
    {
        if (x - 1 >= 0)
        {
            if (blocks[x - 1, y, z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    //x + 1
    private bool RightSide(int x, int y, int z)
    {
        if (x + 1 < chunkWidth)
        {
            if (blocks[x + 1, y, z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }
    #endregion
}