﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public Transform player;
    public Transform globalScriptHolder;

    private int chunkWidth, chunkHeight;
    private Dictionary<string, GameObject> activeChunks = new Dictionary<string, GameObject>();
    private string playerChunk;
    private int visibleDistance = 5;
    private bool refreshChunks = false;

    // Use this for initialization
    void Start()
    {
        chunkWidth = 32;
        chunkHeight = 64;
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player").transform;

        CreateChunks();
    }

    void Update()
    {
        RefreshPlayerChunk();
        if (refreshChunks)
            RefreshChunksList();
    }

    #region cube interaction
    public void CreateBlock(Vector3 blockPos, short blockType)
    {
        int chunkX = Mathf.FloorToInt(blockPos.x / chunkWidth);
        int chunkZ = Mathf.FloorToInt(blockPos.z / chunkWidth);
        string chunkID = chunkX + "#" + chunkZ;

        int blockX = Mathf.RoundToInt(blockPos.x % chunkWidth);
        if (blockX < 0) blockX += chunkWidth;
        int blockY = Mathf.RoundToInt(blockPos.y) + 1;
        int blockZ = Mathf.RoundToInt(blockPos.z % chunkWidth);
        if (blockZ < 0) blockZ += chunkWidth;

        if (activeChunks.ContainsKey(chunkID))
        {
            activeChunks[chunkID].GetComponent<Chunk>().CreateBlock(blockX, blockY, blockZ, blockType);
        }
        else
        {
            Debug.Log("Something's wrong, chunk not found!");
        }
    }

    public void DestroyBlock(Vector3 blockPos, short damage)
    {
        int chunkX = Mathf.FloorToInt(blockPos.x / chunkWidth);
        int chunkZ = Mathf.FloorToInt(blockPos.z / chunkWidth);
        string chunkID = chunkX + "#" + chunkZ;

        int blockX = Mathf.RoundToInt(blockPos.x % chunkWidth);
        if (blockX < 0) blockX += chunkWidth;
        int blockY = Mathf.RoundToInt(blockPos.y) + 1;
        int blockZ = Mathf.RoundToInt(blockPos.z % chunkWidth);
        if (blockZ < 0) blockZ += chunkWidth;

        if (activeChunks.ContainsKey(chunkID))
        {
            activeChunks[chunkID].GetComponent<Chunk>().DestroyBlock(blockX, blockY, blockZ, damage);
        }
        else
        {
            Debug.Log("Something's wrong, chunk not found!");
        }
    }
    #endregion

    #region chunk management
    private void RefreshChunksList()
    {
        //the idea is that world does not need to know directly which chunks are loaded
        //relative x, z for chunk ID
        int x = Mathf.FloorToInt(player.transform.position.x / chunkWidth);
        int z = Mathf.FloorToInt(player.transform.position.z / chunkWidth);

        //create list of *should be active* chunk ids
        List<string> toActivate = new List<string>();
        for (int iz = z - visibleDistance; iz <= z + visibleDistance; iz++)
        {
            for (int ix = x - visibleDistance; ix <= x + visibleDistance; ix++)
            {
                toActivate.Add(ix + "#" + iz);
            }
        }
        //create list of *should be destroyed* chunk ids
        List<string> toRemove = new List<string>();
        foreach (var id in activeChunks)
        {
            if (!toActivate.Contains(id.Key))
            {
                toRemove.Add(id.Key);
            }
        }

        //remove and destroy obsolete chunks and keys
        for (int removeKey = 0; removeKey < toRemove.Count; removeKey++)
        {
            Destroy(activeChunks[toRemove[removeKey]]); //this would be nicer in callback from chunk when it unloads itself maybe?
            activeChunks.Remove(toRemove[removeKey]);
        }

        //add and create new chunks
        for (int activateKey = 0; activateKey < toActivate.Count; activateKey++)
        {
            if (!activeChunks.ContainsKey(toActivate[activateKey].ToString()))
            {
                InitChunk(toActivate[activateKey].ToString());
            }
        }
        refreshChunks = false;
    }

    private void InitChunk(string chunkID)
    {
        //create specific chunk
        activeChunks.Add(chunkID, Instantiate(Resources.Load("Chunk")) as GameObject);
        string[] coords = chunkID.Split('#');
        int x = int.Parse(coords[0]);
        int z = int.Parse(coords[1]);
        activeChunks[chunkID].GetComponent<Chunk>().SetChunkProps(chunkID, chunkWidth, chunkHeight, new Vector3(x * chunkWidth, 0, z * chunkWidth));
    }

    private void RefreshPlayerChunk()
    {
        int x = Mathf.FloorToInt(player.transform.position.x / chunkWidth);
        int z = Mathf.FloorToInt(player.transform.position.z / chunkWidth);
        string currentChunk = x + "#" + z;
        if (currentChunk != playerChunk)
        {
            playerChunk = currentChunk;
            refreshChunks = true;
        }
    }

    private void CreateChunks()
    {
        int x = Mathf.FloorToInt(player.transform.position.x / chunkWidth);
        int z = Mathf.FloorToInt(player.transform.position.z / chunkWidth);

        playerChunk = x + "#" + z;

        //create list of *should be active* chunk ids
        List<string> toActivate = new List<string>();
        for (int iz = x - visibleDistance; iz <= x + visibleDistance; iz ++)
        {
            for (int ix = z - visibleDistance; ix <= z + visibleDistance; ix ++)
            {
                toActivate.Add(ix + "#" + iz);
            }
        }

        //add and create new chunks
        for (int activateKey = 0; activateKey < toActivate.Count; activateKey++)
        {
            if (!activeChunks.ContainsKey(toActivate[activateKey].ToString()))
            {
                InitChunk(toActivate[activateKey].ToString());
            }
        }
    }
    #endregion

    #region blockSet
    /*
    //separate set can be set for each biome
    private void InitBlocks()
    {
        //prepare data - better would be feed it from some xml or something
        List<Vector2> texture = new List<Vector2>();
        texture.Add(new Vector2(0.5f, 0.5f));
        texture.Add(new Vector2(0.5f, 1));
        texture.Add(new Vector2(1, 0.5f));
        texture.Add(new Vector2(1, 1));
        blocks.Add(new Block(1, 1, "stone", texture));

        texture.Clear();
        texture.Add(new Vector2(0, 0));
        texture.Add(new Vector2(0, 0.5f));
        texture.Add(new Vector2(0.5f, 0));
        texture.Add(new Vector2(0.5f, 0.5f));
        blocks.Add(new Block(2, 2, "grass", texture));

        texture.Clear();
        texture.Add(new Vector2(0, 0.5f));
        texture.Add(new Vector2(0, 1));
        texture.Add(new Vector2(0.5f, 0.5f));
        texture.Add(new Vector2(0.5f, 1));
        blocks.Add(new Block(3, 3, "grass", texture));

        texture.Clear();
        texture.Add(new Vector2(0.5f, 0));
        texture.Add(new Vector2(0.5f, 0.5f));
        texture.Add(new Vector2(1, 0));
        texture.Add(new Vector2(1, 0.5f));
        blocks.Add(new Block(3, 3, "grass", texture));
    }
    */
    #endregion
}
