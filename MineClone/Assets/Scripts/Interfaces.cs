﻿public interface IInventory
{
    void AddItem(Item i);
    void AddItem(Item i, int count);
    void RemoveItem(Item i);
    void RemoveItem(Item i, int count);
}

public interface IChunk
{
    void CreateBlock(int blockX, int blockY, int blockZ, byte blockType);
    void DestroyBlock(int blockX, int blockY, int blockZ, int damage);
}

public interface IItem
{
    short GetItemType();
}

public interface IItemSpawner
{
    void SpawnItem(Item i);
    void SpawnItem(Item i, int count);
}