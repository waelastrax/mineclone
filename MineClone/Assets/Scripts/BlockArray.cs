﻿using UnityEngine;

public class BlockArray
{
    private short[] blocks;

    public BlockArray(short[] blocks)
    {
        Debug.Log("Initializing from array.");
        this.blocks = blocks;
    }

    public BlockArray(int x, int y, int z)
    {
        Debug.Log("Initializing from size.");
        blocks = new short[x * y * x];
    }

    public short GetBlock(ArrayPosition pos)
    {
        if (pos.CheckIdValue(blocks.Length))
        {
            Debug.Log("array length: " + blocks.Length);
            Debug.Log(pos.ToString());
            return blocks[pos.x * pos.y * pos.z];
        }
        else
        {
            Debug.Log("Invalid array index: " + pos.ToString());
            return -1;
        }   
    }

    public void SetBlock(ArrayPosition pos, short value)
    {
        blocks[pos.x * pos.y * pos.z] = value;
    }

    public short[] Blocks
    {
        get
        {
            return blocks;
        }
    }

    public bool IsEmpty(ArrayPosition pos)
    {
        if (pos.CheckIdValue(blocks.Length))
        {
            if (blocks[pos.x * pos.y * pos.z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
}
