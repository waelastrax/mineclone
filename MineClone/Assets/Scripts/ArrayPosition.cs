﻿using UnityEngine;

public class ArrayPosition
{
    public int x, y, z;

    public ArrayPosition(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public ArrayPosition(ArrayPosition pos)
    {
        x = pos.x;
        y = pos.y;
        z = pos.z;
    }

    public ArrayPosition(Vector3 vec)
    {
        x = Mathf.RoundToInt(vec.x);
        y = Mathf.RoundToInt(vec.y);
        z = Mathf.RoundToInt(vec.z);
    }

    public bool Equals(ArrayPosition pos)
    {
        if (x == pos.x & y == pos.y & z == pos.z)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckIdValue(int value)
    {
        return x * y * z < value ? true : false;
    }

    public bool CheckUp(int i)
    {
        return y + 1 <= i ? true : false;
    }

    public ArrayPosition Up
    {
        get
        {
            return new ArrayPosition(x, y + 1, z);
        }
    }

    public bool CheckDown(int i)
    {
        return y - 1 >= i ? true : false;
    }

    public ArrayPosition Down
    {
        get
        {
            return new ArrayPosition(x, y - 1, z);
        }
    }

    public bool CheckRight(int i)
    {
        return x + 1 <= i ? true : false;
    }

    public ArrayPosition Right
    {
        get
        {
            return new ArrayPosition(x + 1, y, z);
        }
    }

    public bool CheckLeft(int i)
    {
        return x - 1 >= i ? true : false;
    }

    public ArrayPosition Left
    {
        get
        {
            return new ArrayPosition(x - 1, y, z);
        }
    }

    public bool CheckFront(int i)
    {
        return z + 1 <= i ? true : false;
    }

    public ArrayPosition Front
    {
        get
        {
            return new ArrayPosition(x, y, z + 1);
        }
    }

    public bool CheckBack(int i)
    {
        return z - 1 >= i ? true : false;
    }

    public ArrayPosition Back
    {
        get
        {
            return new ArrayPosition(x, y, z - 1);
        }
    }

    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }

    public override string ToString()
    {
        return "x: " + x + ", y: " + y + " z: " + z;
    }
}
