﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class Main : MonoBehaviour
{
    public Transform button;

    private bool load = false;
    private string worldFolder = "Save/world001/"; //this should be passed down to world if selected in menu

    void Start()
    {
        if (!SaveDataAvailable())
            button.GetComponent<Button>().interactable = false;
        BlockIO.SetWorldFolder(worldFolder);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    public void NewGame()
    {
        ClearSaveData();
        GameObject.DontDestroyOnLoad(this);
        SceneManager.LoadScene("mainScene");
    }

    public void LoadGame()
    {
        load = true;
        GameObject.DontDestroyOnLoad(this);
        SceneManager.LoadScene("mainScene");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public string GetWorldFolder()
    {
        return worldFolder;
    }

    private void ClearSaveData()
    {
        if (Directory.Exists(worldFolder))
        {
            Directory.Delete(worldFolder, true);
        }
        Directory.CreateDirectory(worldFolder);
    }

    private bool SaveDataAvailable()
    {
        if (Directory.Exists(worldFolder))
        {
            if (File.Exists(worldFolder + "0#0.trz")) //this file *should* always exist if the world was created
            {
                Debug.Log("Save data available");
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            Directory.CreateDirectory(worldFolder);
            return false;
        }
        
    }
}
