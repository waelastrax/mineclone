﻿public class DummyBlock
{
    private int x, y, z;
    private short blockType;
    private short blockHealth;

    public DummyBlock(int x, int y, int z, short bt)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.blockType = bt;
        SetBlockHealth(blockType);
    }

    private void SetBlockHealth(int bt)
    {
        switch (bt)
        {
            case 1:
                this.blockHealth = 100;
                break;
            case 2:
                this.blockHealth = 1;
                break;
            case 3:
                this.blockHealth = 2;
                break;
            case 4:
                this.blockHealth = 3;
                break;
            default:
                break;
        }
    }

    public bool SameBlock(int newX, int newY, int newZ)
    {
        return newX == x && newY == y && newZ == z ? true : false;
    }

    public void DamageBlock(short damage)
    {
        blockHealth -= damage;
    }

    public bool IsDestroyed()
    {
        return  blockHealth == 0 ? true : false;
    }
}
