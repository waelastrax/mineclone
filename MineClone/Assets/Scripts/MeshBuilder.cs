﻿using System.Collections.Generic;
using UnityEngine;

public static class MeshBuilder
{
    private static Mesh mesh;
    private static MeshCollider meshCollider;
    private static List<Vector3> newVertices = new List<Vector3>();
    private static List<Vector2> newUV = new List<Vector2>();
    private static List<int> newTriangles = new List<int>();

    static int chunkWidth, chunkHeight;
    static short[,,] blocks;

    public static void GetTexture()
    {
        
    }




    #region mesh handling
    private static void GenerateMesh(short [,,] blocks)
    {
        mesh.Clear();
        newVertices.Clear();
        newTriangles.Clear();
        newUV.Clear();
        for (int y = 0; y < chunkHeight; y++)
        {
            for (int z = 0; z < chunkWidth; z++)
            {
                for (int x = 0; x < chunkWidth; x++)
                {
                    if (blocks[x, y, z] > 0)
                    {
                        //this does not feel very efficient
                        if (TopSide(x, y, z))
                        {
                            DrawSide(new Vector3(x, y, z), Vector3.forward, Vector3.right, blocks[x, y, z]);
                        }
                        if (BottomSide(x, y, z))
                        {
                            DrawSide(new Vector3(x + 1, y - 1, z), Vector3.forward, Vector3.left, blocks[x, y, z]);
                        }
                        if (BackSide(x, y, z))
                        {
                            DrawSide(new Vector3(x + 1, y, z), Vector3.down, Vector3.left, blocks[x, y, z]);
                        }
                        if (FrontSide(x, y, z))
                        {
                            DrawSide(new Vector3(x, y, z + 1), Vector3.down, Vector3.right, blocks[x, y, z]);
                        }
                        if (LeftSide(x, y, z))
                        {
                            DrawSide(new Vector3(x, y, z), Vector3.down, Vector3.forward, blocks[x, y, z]);
                        }
                        if (RightSide(x, y, z))
                        {
                            DrawSide(new Vector3(x + 1, y, z + 1), Vector3.down, Vector3.back, blocks[x, y, z]);
                        }
                    }
                }
            }
        }
        mesh.vertices = newVertices.ToArray();
        mesh.triangles = newTriangles.ToArray();
        mesh.uv = newUV.ToArray();
        mesh.RecalculateBounds();
        //mesh.RecalculateNormals(); - this screws the mesh totally and royally
        meshCollider.sharedMesh = null;
        meshCollider.sharedMesh = mesh;
    }

    private static void DrawSide(Vector3 start, Vector3 sideA, Vector3 sideB, short blockType)
    {
        int index = newVertices.Count;

        newVertices.Add(start);
        newVertices.Add(start + sideA);
        newVertices.Add(start + sideB);
        newVertices.Add(start + sideA + sideB);

        for (int i = 0; i < 3; i++) newTriangles.Add(index + i);
        for (int i = 3; i > 0; i--) newTriangles.Add(index + i);

        switch (blockType)
        {
            case 1: //stone
                newUV.Add(new Vector2(0.5f, 0.5f));
                newUV.Add(new Vector2(0.5f, 1));
                newUV.Add(new Vector2(1, 0.5f));
                newUV.Add(new Vector2(1, 1));
                break;
            case 2: //grass 
                newUV.Add(new Vector2(0, 0));
                newUV.Add(new Vector2(0, 0.5f));
                newUV.Add(new Vector2(0.5f, 0));
                newUV.Add(new Vector2(0.5f, 0.5f));
                break;
            case 3: //grass
                newUV.Add(new Vector2(0, 0.5f));
                newUV.Add(new Vector2(0, 1));
                newUV.Add(new Vector2(0.5f, 0.5f));
                newUV.Add(new Vector2(0.5f, 1));
                break;
            case 4: //dirt
                newUV.Add(new Vector2(0.5f, 0));
                newUV.Add(new Vector2(0.5f, 0.5f));
                newUV.Add(new Vector2(1, 0));
                newUV.Add(new Vector2(1, 0.5f));
                break;
            default:
                break;
        }
    }

    private static bool TopSide(int x, int y, int z)
    {
        if (y + 1 < chunkHeight)
        {
            if (blocks[x, y + 1, z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    private static bool BottomSide(int x, int y, int z)
    {
        if (y - 1 >= 0)
        {
            if (blocks[x, y - 1, z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    //z + 1
    private static bool FrontSide(int x, int y, int z)
    {
        if (z + 1 < chunkWidth)
        {
            if (blocks[x, y, z + 1] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    //z - 1
    private static bool BackSide(int x, int y, int z)
    {
        if (z - 1 >= 0)
        {
            if (blocks[x, y, z - 1] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    //x - 1
    private static bool LeftSide(int x, int y, int z)
    {
        if (x - 1 >= 0)
        {
            if (blocks[x - 1, y, z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }

    //x + 1
    private static bool RightSide(int x, int y, int z)
    {
        if (x + 1 < chunkWidth)
        {
            if (blocks[x + 1, y, z] == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; //not sure whether this is necessary
        }
    }
    #endregion
}
