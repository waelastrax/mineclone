﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class BlockIO
{
    private static string worldFolder; //get the world folder on load

    public static void SetWorldFolder(string wf)
    {
        worldFolder = wf;
    }

    #region old save&load
    public static void SaveChunkData(string chunkID, short [,,] blocks)
    {
        if (File.Exists(worldFolder + chunkID + ".trz"))
        {
            File.Delete(worldFolder + chunkID + ".trz");
        }
        BinaryFormatter bf = new BinaryFormatter();
        using (FileStream fs = new FileStream(worldFolder + chunkID + ".trz", FileMode.Create, FileAccess.Write))
        {
            bf.Serialize(fs, blocks);
        }
    }

    public static short [,,] LoadChunkData(string chunkID)
    {
        if (File.Exists(worldFolder + chunkID + ".trz") && worldFolder != string.Empty)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream fs = new FileStream(worldFolder + chunkID + ".trz", FileMode.Open, FileAccess.Read))
            {
                return (short[,,])bf.Deserialize(fs);
            }
        }
        else
        {
            return null;
        }
    }
    #endregion

    #region new format save&load
    public static void SaveChunkData(string chunkID, BlockArray blocks)
    {
        if (File.Exists(worldFolder + chunkID + ".cnk"))
        {
            File.Delete(worldFolder + chunkID + ".cnk");
        }
        BinaryFormatter bf = new BinaryFormatter();
        using (FileStream fs = new FileStream(worldFolder + chunkID + ".cnk", FileMode.Create, FileAccess.Write))
        {
            bf.Serialize(fs, blocks.Blocks);
        }
    }

    public static short[] LoadBlockArrayData(string chunkID)
    {
        if (File.Exists(worldFolder + chunkID + ".cnk") && worldFolder != string.Empty)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream fs = new FileStream(worldFolder + chunkID + ".cnk", FileMode.Open, FileAccess.Read))
            {
                return (short[])bf.Deserialize(fs);
            }
        }
        else
        {
            return null;

        }
    }
    #endregion
}
